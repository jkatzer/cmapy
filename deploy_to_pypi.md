# How to upload to PyPi

1. Create a source distribution: 

   ```python3 setup.py sdist```
   
1. Test local installation:

   ```pip install --user . --upgrade```
   
   ```pip3 install --user . --upgrade```
   
1. Upload to PyPi:

   ```twine upload dist/*```