#!/usr/bin/env python
"""Colorize an image."""

import cv2
import cmapy


# Read image.
img = cv2.imread("imgs/woman.png")

# Colorize with OpenCV.
img_colorized = cv2.applyColorMap(img, cmapy.cmap("viridis"))

# Or: colorize directly with Cmapy (does the same thing).
img_colorized = cmapy.colorize(img, "viridis")

# Display.
cv2.imshow("Image", img_colorized)
cv2.waitKey(0)
